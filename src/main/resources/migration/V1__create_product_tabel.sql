create table  if not exists product(
    id bigint primary key auto_increment,
    name varchar(32) not null ,
    price decimal not null ,
    unit varchar(4) not null ,
    img_url varchar(256) not null
)ENGINE = InnoDB;