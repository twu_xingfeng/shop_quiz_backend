alter table product convert to character set utf8;
alter table product modify name varchar(32) character set utf8;
alter table product modify unit varchar(4) character set utf8;
alter table product modify img_url varchar(256) character set utf8;