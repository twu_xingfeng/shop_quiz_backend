create table if not exists cart(
    id bigint primary key auto_increment,
    product_id bigint not null,
    number bigint not null,
    foreign key(product_id) references product(id)
) ENGINE = InnoDB;

