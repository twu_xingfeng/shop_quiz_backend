package com.twuc.webApp.contract;

import java.math.BigDecimal;

public class OrderResult {
    private Long id;
    private String name;
    private BigDecimal price;
    private Long number;
    private String unit;

    public OrderResult() {
    }

    public OrderResult(Long id, String name, BigDecimal price, Long number, String unit) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.number = number;
        this.unit = unit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "OrderResult{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", number=" + number +
                ", unit='" + unit + '\'' +
                '}';
    }
}
