package com.twuc.webApp.handlers;

import com.twuc.webApp.exceptions.ProductExistsException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static com.twuc.webApp.constant.CONSTANT.MSG_PRODUCT_EXISTS;

@ControllerAdvice
public class ExceptionsHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = {ProductExistsException.class})
    protected ResponseEntity<Object> productExists(
            RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex,MSG_PRODUCT_EXISTS,
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}
