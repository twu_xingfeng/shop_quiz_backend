package com.twuc.webApp.controller;

import com.twuc.webApp.contract.OrderResult;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.service.impl.ProductServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class ShopController {

    private final ProductServiceImpl productService;

    public ShopController(ProductServiceImpl productService) {
        this.productService = productService;
    }

    @GetMapping("/api/products")
    public ResponseEntity<List<Product>> getAllProducts(){
        return ResponseEntity.status(HttpStatus.OK).body(productService.getAllProduct());
    }

    @PostMapping("/api/products")
    public ResponseEntity addProduct(@RequestBody Product product){
        return ResponseEntity.status(HttpStatus.CREATED).body(productService.addProduct(product));
    }

    @PostMapping("/api/products/{pid}")
    public ResponseEntity addCart(@PathVariable("pid") Long productId){
        return ResponseEntity.status(HttpStatus.CREATED).body(productService.addToCart(productId));
    }

    @GetMapping("/api/cart")
    public ResponseEntity<List<OrderResult>> getAllOrders(){
        return ResponseEntity.status(HttpStatus.OK).body(productService.getOrders());
    }

    @DeleteMapping("/api/cart/{cid}")
    public ResponseEntity deleteCart(@PathVariable("cid") Long cartId){
        return ResponseEntity.status(HttpStatus.CREATED).body(productService.deleteCart(cartId));
    }
}
