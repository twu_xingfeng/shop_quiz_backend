package com.twuc.webApp.repository;

import com.twuc.webApp.domain.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<CartItem,Long> {
    Optional<CartItem> findFirstByProductId(Long productId);
}
