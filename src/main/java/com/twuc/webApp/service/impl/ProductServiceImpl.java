package com.twuc.webApp.service.impl;

import com.twuc.webApp.contract.OrderResult;
import com.twuc.webApp.domain.CartItem;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.exceptions.ProductExistsException;
import com.twuc.webApp.repository.OrderRepository;
import com.twuc.webApp.repository.ProductRepository;
import com.twuc.webApp.service.IProductService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.twuc.webApp.constant.CONSTANT.MSG_PRODUCT_EXISTS;

@Service
public class ProductServiceImpl implements IProductService {

    private final ProductRepository productRepository;

    private final OrderRepository orderRepository;

    public ProductServiceImpl(ProductRepository productRepository, OrderRepository orderRepository) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
    }

    @Override
    public Product addProduct(Product product) {
        List<Product> products = productRepository.findByName(product.getName());
        if (products.size() != 0) {
            throw new ProductExistsException(MSG_PRODUCT_EXISTS);
        }
        return productRepository.save(product);
    }

    @Override
    public List<Product> getAllProduct() {
        return productRepository.findAll();
    }

    @Override
    public CartItem addToCart(Long productId) {
        Optional<CartItem> optionalCart = orderRepository.findFirstByProductId(productId);
        CartItem cart = new CartItem(productId, 1L);
        if (optionalCart.isPresent()) {
            cart.setId(optionalCart.get().getId());
            cart.setNumber(optionalCart.get().getNumber() + 1);
        }
        return orderRepository.save(cart);
    }

    @Override
    public List<OrderResult> getOrders() {
        List<CartItem> carts = orderRepository.findAll();
        List<OrderResult> orderResults = new ArrayList<>();
        for (CartItem cart : carts) {
            Optional<Product> productOptional = productRepository.findById(cart.getProductId());
            Product product = productOptional.orElseThrow(RuntimeException::new);
            orderResults.add(new OrderResult(cart.getId(), product.getName(), product.getPrice(), cart.getNumber(), product.getUnit()));
        }
        return orderResults;
    }

    @Override
    public CartItem deleteCart(Long cartId) {
        CartItem cart = orderRepository.findById(cartId).orElseThrow(RuntimeException::new);
        orderRepository.delete(cart);
        return cart;
    }

}
