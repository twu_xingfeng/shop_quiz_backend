package com.twuc.webApp.service;

import com.twuc.webApp.contract.OrderResult;
import com.twuc.webApp.domain.CartItem;
import com.twuc.webApp.domain.Product;

import java.util.List;

public interface IProductService {
    Product addProduct(Product kele);

    List<Product> getAllProduct();

    CartItem addToCart(Long productId);

    List<OrderResult> getOrders();

    CartItem deleteCart(Long cartId);
}
