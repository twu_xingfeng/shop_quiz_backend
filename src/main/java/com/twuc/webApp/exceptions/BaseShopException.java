package com.twuc.webApp.exceptions;

import static com.twuc.webApp.constant.CONSTANT.SOMETHING_WENT_WRONG;

public class BaseShopException extends RuntimeException {
    public BaseShopException() {
    }

    public BaseShopException(String message) {
        super(SOMETHING_WENT_WRONG+message);
    }
}
