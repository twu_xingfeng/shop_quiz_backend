package com.twuc.webApp.exceptions;

public class ProductExistsException extends BaseShopException {
    public ProductExistsException() {
    }

    public ProductExistsException(String message) {
        super(message);
    }
}
