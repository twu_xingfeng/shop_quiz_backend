package com.twuc.webApp.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.domain.Product;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class TestFile {
    @Test
    void genJson() throws JsonProcessingException {
        Product kele = new Product("可乐",new BigDecimal(3),"元","https://www.hingwongmarket.ie/wp-content/uploads/2017/12/391.Coca-Cola-330ml-%E5%8F%AF%E4%B9%90.jpg");
        Product huotui = new Product("火腿肠",new BigDecimal(12),"元","https://cdn.shopify.com/s/files/1/0012/0324/1007/products/cover4.jpg?v=1540489484");
        Product latiao = new Product("卫龙辣条",new BigDecimal(1.5),"元","https://cdn.shopify.com/s/files/1/1029/9385/products/TB2bMw7nwRkpuFjy1zeXXc.6FXa__1673846591_large.jpg?v=1511754940");

        System.out.println(new ObjectMapper().writeValueAsString(kele));
        System.out.println(new ObjectMapper().writeValueAsString(huotui));
        System.out.println(new ObjectMapper().writeValueAsString(latiao));
    }
}
