package com.twuc.webApp.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.base.JpaTestBase;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.service.impl.ProductServiceImpl;
import com.twuc.webApp.utils.JSONUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

class ShopControllerTest extends JpaTestBase {

    @Autowired
    private ProductServiceImpl productService;

    @Test
    void should_get_all_products() throws Exception {
        createSomeProductsForBelowTest();

        String contentAsString = getAllProductsFromApi().andExpect(MockMvcResultMatchers.status().is(200)).andReturn().getResponse().getContentAsString();

        List<Product> products = JSONUtils.fromJSON(new TypeReference<List<Product>>() {
        }, contentAsString);


        assertEquals(products.size(),3);
    }

    @Test
    void should_create_product() throws Exception {
        Product product = new Product("可乐",new BigDecimal(1.5),"元","https://www.hingwongmarket.ie/wp-content/uploads/2017/12/391.Coca-Cola-330ml-%E5%8F%AF%E4%B9%90.jpg");
        createProductFromApi(product).andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_throw_when_create_exists_product() throws Exception {
        createSomeProductsForBelowTest();

        Product product = new Product("可乐",new BigDecimal(1.5),"元","https://www.hingwongmarket.ie/wp-content/uploads/2017/12/391.Coca-Cola-330ml-%E5%8F%AF%E4%B9%90.jpg");
        createProductFromApi(product).andExpect(MockMvcResultMatchers.status().is(400));
    }


    private void createSomeProductsForBelowTest() {
        Product kele = new Product("可乐",new BigDecimal(3),"元","https://www.hingwongmarket.ie/wp-content/uploads/2017/12/391.Coca-Cola-330ml-%E5%8F%AF%E4%B9%90.jpg");
        Product huotui = new Product("火腿肠",new BigDecimal(12),"元","https://cdn.shopify.com/s/files/1/0012/0324/1007/products/cover4.jpg?v=1540489484");
        Product latiao = new Product("卫龙辣条",new BigDecimal(1.5),"元","https://cdn.shopify.com/s/files/1/1029/9385/products/TB2bMw7nwRkpuFjy1zeXXc.6FXa__1673846591_large.jpg?v=1511754940");

        productService.addProduct(kele);
        productService.addProduct(huotui);
        productService.addProduct(latiao);
    }


    private ResultActions createProductFromApi(Product product) throws Exception {
        return  getMockMvc().perform(post("/api/products").contentType(MediaType.APPLICATION_JSON_UTF8).content(
                new ObjectMapper().writeValueAsString(product)
        ));
    }


    private ResultActions getAllProductsFromApi() throws Exception {
        return  getMockMvc().perform(get("/api/products"));
    }
}